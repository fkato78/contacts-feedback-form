<?php
/**
 * Class Contact
 */

class Contact
{
    private $name;
    private $email;
    private $site;
    private $message;
    private $file;
    private $created_at ;

    public function __construct(
        $name,
        $email,
        $site,
        $message,
        $file,
        $created_at
    )
    {
        $this->name = $name;
        $this->email = $email;
        $this->site = $site;
        $this->message = $message;
        $this->file = $file;
        $this->created_at = $created_at;
    }

    /**
     * To access private attributes
     * @param $prop
     * @return mixed
     */
    public function __get($prop)
    {
        return $this->$prop;
    }


}