<?php
/**
 * Error handling
 */

    $errors = [];
    if(!defined('DS')) {
        define('DS', DIRECTORY_SEPARATOR);
    };

//    name field errors
    $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
    if(empty($name)){
        $errors['name'] = 'You forgot to enter your name';
    } elseif (strlen($name) < 3){
        $errors['name'] = 'Name must have 3 characters at least';
    }

//    email field errors
$email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
    if(empty($email)){
        $errors['email'] = 'You forgot to enter your email address';
    } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)){
        $errors['email'] = 'the e-mail format is incorrect.';
    }

//    site field errors
$site = filter_input(INPUT_POST, 'site', FILTER_SANITIZE_URL);
    if(empty($site)){
        $errors['site'] = "Please insert the site";
    } elseif (!filter_var($site, FILTER_VALIDATE_URL)){
        $errors['site'] = ' the url format is incorrect.';
    }

//    message field errors
$message = filter_input(INPUT_POST, 'message', FILTER_SANITIZE_STRING);
    if(empty($message)){
        $errors['message'] = "Please insert message";
    } elseif (strlen($message ) < 10){
        $errors['message'] = "The min length of your message is 10 characters";
    }

//    image field errors
    $extensions = ['jpg', 'png'];
    if(isset($_FILES['file'])){
        $image = $_FILES['file'];
        $image_name = $image['name'];
        $image_ext = pathinfo($image_name, PATHINFO_EXTENSION);
        $image_tmp_dir = $image['tmp_name'];
        $target_folder = __DIR__ . DS . "uploads";
        // Create the target folder if not exists
        if(!file_exists($target_folder)){
            mkdir($target_folder, 0777, true);
        }
        $target_file = $target_folder. DS . basename($image_name);
        $image_size = $image['size'];
        if(!in_array($image_ext, $extensions)){
            $errors['file'] = "Please upload file with extension(jpg, png)";
        } elseif ($image_size > 10000000){
            $errors['file'] = "File is too large";
        } else {
            move_uploaded_file( $image_tmp_dir, $target_file);
        }

    }




