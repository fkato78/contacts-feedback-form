<?php
    require_once 'db.php';
    require_once 'Contact.php';
    $success = null;
    include_once 'includes/_insert_record.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Contact us</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>
<body>
<div class="container mt-4">
    <?php if(!is_null($success)): ?>
        <div class="alert alert-success mt-4" role="alert">
            <?php echo $success; ?>
        </div>
    <?php endif; ?>
    <div class="row">
            <?php include_once 'includes/_form.php'?>
            <div class="mt-4 col-2"></div>
            <div class="mt-4 col-5">
                <?php
                /**
                 * Select record from the database
                 */
                $sql = "SELECT * FROM contacts";
                // if user search for message
                if (isset($_POST['search_message'])) {
                    $search_message = filter_input(INPUT_POST, 'search_message', FILTER_SANITIZE_STRING);
                    $sql = "SELECT * FROM contacts WHERE message LIKE '%".$search_message."%'";

                }
                $stmt = $connection->query($sql);
                // Use ORM(map each record to class)
                $objConstructorArgs = array('name', 'email', 'site', 'message', 'file', 'created_at');
                $result = $stmt->fetchAll(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'Contact', $objConstructorArgs);
                $result = (is_array($result) && !empty($result)) ? $result : false;
                ?>
                <h1 class="text-center mb-2">List of contacts</h1>
                <hr>
                <form action="" method="post">
                    <input type="text" class="form-control" name="search_message" placeholder="Search in message" />

                </form>
                <?php include_once 'includes/_contacts_table.php'?>
            </div>
    </div>
</div>
<?php include_once 'includes/_js.php'?>
</body>
</html>


