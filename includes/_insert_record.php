<?php
if($_SERVER['REQUEST_METHOD'] === "POST"){
    require_once 'errors.php';
    if(isset($_POST['submit'])){
        if(empty($errors)){
            /**
             * Insert record in database
             */

            $data = [
                ':name' => $name,
                ':email' => $email,
                ':site' => $site,
                ':message' => $message,
                ':file' => $image_name,
            ];
            // create the sql query
            $sql = "INSERT INTO contacts SET name = :name, email = :email, site = :site, message = :message, file = :file, created_at = Now()";
            // prepare the query
            $stmt = $connection->prepare($sql);
            // execute the query to insert new record
            if($stmt->execute($data) === true){
                $success = "Record inserted successfully!";
            }
        }
    }
}

