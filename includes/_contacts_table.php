<table class="table">
    <thead>
    <tr>
        <th>Image</th>
        <th>message</th>
        <th>Date</th>
    </tr>
    </thead>
    <tbody>
    <?php if(false !== $result): ?>
        <?php foreach ($result as $contact): ?>
            <tr>
                <td><img width="100" src="<?php echo "./uploads/" . $contact->file ?>" alt=""></td>
                <td><?php echo $contact->message ?></td>
                <td><?php echo $contact->created_at ?></td>
            </tr>
        <?php endforeach; ?>
    <?php else: ?>
        <td colspan="5"><p class="text-center">No data to list</p></td>
    <?php endif; ?>
    </tbody>
</table>
