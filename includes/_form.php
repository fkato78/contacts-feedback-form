<div class="mt-4 col-5">
    <h1 class="text-center mb-2">Contact Us</h1>
    <hr>
    <form method="post" enctype="multipart/form-data">
        <div class="form-group row">
            <label for="name" class="col-lg-2 col-form-label">Name</label>
            <div class="col-lg-10">
                <input type="text" name="name" class="form-control" id="name" aria-describedby="nameHelp"  placeholder="Name (Min 3 characters)">
                <?php if(isset($_POST['name']) && (!empty($errors['name']))): ?>
                    <small id="nameHelp" class="form-text p-1 mb-2 bg-danger text-white"><?php echo $errors['name']; ?></small>
                <?php endif; ?>
            </div>
        </div>
        <div class="form-group row">
            <label for="email" class="col-lg-2 col-form-label">Email</label>
            <div class="col-lg-10">
                <input type="email" name="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="email">
                <?php if(isset($_POST['email']) && (!empty($errors['email']))): ?>
                    <small id="emailHelp" class="form-text p-1 mb-2 bg-danger text-white"><?php echo $errors['email'];  ?></small>
                <?php endif; ?>
            </div>
        </div>
        <div class="form-group row">
            <label for="site" class="col-lg-2 col-form-label">Site</label>
            <div class="col-lg-10">
                <input type="text" name="site" class="form-control" aria-describedby="siteHelp" id="site" placeholder="Site">
                <?php if(isset($_POST['site']) && (!empty($errors['site']))): ?>
                    <small id="siteHelp" class="form-text p-1 mb-2 bg-danger text-white"><?php echo $errors['site'];  ?></small>
                <?php endif; ?>
            </div>
        </div>
        <div class="form-group row">
            <label for="message" class="col-lg-2 col-form-label">Message</label>
            <div class="col-lg-10">
                <textarea name="message" class="form-control" id="message" rows="3" placeholder="Message (Min 10 characters) "></textarea>
                <?php if(isset($_POST['message']) && (!empty($errors['message']))): ?>
                    <small class="form-text p-1 mb-2 bg-danger text-white"><?php echo $errors['message'];  ?></small>
                <?php endif; ?>
            </div>
        </div>
        <div class="form-group row">
            <label for="file" class="col-lg-2 col-form-label">Image</label>
            <div class="col-lg-10">
                <input type="file" name="file" class="form-control-file" id="file">
                <?php if(isset($_FILES['file']) && (!empty($errors['file']))): ?>
                    <small  class="form-text p-1 mb-2 bg-danger text-white"><?php echo $errors['file'];  ?></small>
                <?php endif; ?>
            </div>
        </div>
        <button type="submit" name="submit" class="btn btn-secondary btn-lg btn-block">Submit</button>
    </form>
</div>
