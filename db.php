<?php
/**
 * Connect to DB and create contacts table
 */

require_once 'config.php';
$connection = null;
$options = array(
    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8',
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
);
try{
    $connection = new PDO("mysql://hostname=" . DB_HOST. ";dbname=" . DB_NAME, DB_USER, DB_PASSWORD, $options);

    // Create table contacts
    // sql query to create table
    $sql = "CREATE TABLE IF NOT EXISTS contacts (
        id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
        name VARCHAR(55) NOT NULL,
        email VARCHAR(55) NOT NULL,
        site VARCHAR(50) NOT NULL,
        file TEXT,
        message TEXT,
        created_at DATE 
    )";
    $connection->exec($sql);

} catch (PDOException $ex){
    echo $ex->getMessage();
}